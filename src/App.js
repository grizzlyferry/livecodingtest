import React, { Component } from 'react';
import './App.css';
import { Boxlist } from './component/boxlist';

class App extends Component{
  constructor(){
    super();

    this.state = {
      colors:[],
      searchField:'',
    };
  }

  componentDidMount(){
    fetch('https://api.npoint.io/047f6da0f63a3887a54d/posts')
    .then(response => response.json())
    .then(posts => this.setState({colors: posts}))
  }

  render(){
    const {colors,searchField} = this.state;
    const filteredColor = colors.filter(color =>
      color.name.toLowerCase().includes(searchField.toLowerCase())
    )
    return(
      <div className='App'>
        <div className='App-Header'><h1>Search Your Color Here :)</h1></div>
        <br />
        
        <input 
        type = 'search' 
        placeholder='search color' 
        onChange={e => this.setState({searchField : e.target.value})} />
        <br /><br />

        <form >
        <select onChange={e => this.setState({searchField : e.target.value})}>
        <option value="red">red</option>
        <option value="green">green</option>
        <option value="yellow">yellow</option>
        <option value="blue">blue</option>
        <option value="brown">brown</option>
        <option value="grey">grey</option>
        <option value="purple">purple</option>
        <option value="pink">pink</option>
        </select>
        </form>
        <br /><br />
        <Boxlist colors ={filteredColor} />
      </div>
    )
  }
}

export default App;
