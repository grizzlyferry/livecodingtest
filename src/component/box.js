import React from 'react'
import './boxlist-styles.css'

export const Box = (props) => (
    <div className = 'box'>
        {props.color.name}
        <h1>{props.color.colorname}</h1>
        Darker = {props.color.darker}
    </div>
)