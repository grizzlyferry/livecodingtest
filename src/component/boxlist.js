import React from 'react';
import './boxlist-styles.css'
import './box'
import { Box } from './box';

export const Boxlist = props => (
    <div className='boxlist'>
        {props.colors.map(color => (
        <Box key={color.id} color={color} />
        ))}
    </div>
)